ArrayList<Drop> drops = new ArrayList<Drop>();


void setup() {
	size(1366,768, P3D);
	frameRate(60);
	background(0, 0, 0);
	noSmooth();
}

void mouseMoved() {
	drops.add(new Drop());

}

void mousePressed(){
        drops = null;
	drops = new ArrayList<Drop>();
	background(0, 0, 0);
}

void draw() {
	background(0, 0, 0);

	for (int i=0; i < drops.size(); i++){
		Drop drop = drops.get(i);
		drop.move();
		drop.dry();
		drop.die();
	}
 //println("drops total: " + drops.size());
 println(frameRate);
 //saveFrame("/Volumes/ramdisk/frames/filename-####.tga");
 //saveFrame("frames/filename-####.tga");
}


class Drop {
	// add in variables you need
	// ex: float color;
	// get variables from outside by calling object.variablename 
	// or set variables from outside by calling object.variablename = foo;
	float mass, x, y, gravity, diam;
	color c;	
	int total, loops, life;

	Drop() {
		// constructor
		// set up the variables with data
		// also load any datafiles (esp. for Processing) that you'll work with
		x = mouseX;
		y = mouseY;
		mass = random(-1, 1);
		strokeWeight(1);
		stroke(255,255,255);
		noFill();
		ellipse(x, y, 10, 10);
		total = 0;
		diam = random(0.1,8);
		gravity = 1.098;
		loops = 0;
		life = int(random(1,88));
	}

	void move(){
		// different methods the class will have
		// use by calling object.function()
		loops++;
		y += mass + pow(gravity,loops);
		
		c = color(255,255,255);
		// println(i + " : " + y);
		fill(c);
		ellipse(x, y, diam, diam);

		
	}

	void die(){
		if (y > height+100) {
			drops.remove(this);
		}
	}

	void dry (){
		if (loops > life){
			// draw an ellipse
			// mass affects the size of the color drop
			// needs to add the dried pixel to another array for redrawing on every frame
			fill(int(random(100,255)),int(random(100,255)),int(random(100,255)));
			noStroke();
			rect(x, y, diam*4, diam*4);
			// kill particle
			drops.remove(this);
		}
	}
}
