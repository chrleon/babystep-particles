# README #

### What is this repository for? ###

* My first attempts at a particle system in processing. Killing of particles offscreen to increase framerate also.

### How do I get set up? ###

Run it inside processing 2.2.1. Not tested in Processing 3. 
If you want to store files to the RAMDISK that I use, I tried RAMDiskCreator by Florian Bogner - https://bogner.sh/2012/12/os-x-create-a-ram-disk-the-easy-way/. I thought it might speed up the saveFrame()-funnction, but only marginally.

### Help wanted ###
If you think my code stinks, tell me! I want to learn from you :)